import { Routes } from '@angular/router';
import { DriversComponent } from './components/drivers-table.component';

export const routes: Routes = [{ path: '', component: DriversComponent, data: { title: 'Drivers' } }];
export default routes;

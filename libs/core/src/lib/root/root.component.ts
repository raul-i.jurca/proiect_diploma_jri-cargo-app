import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { AuthCoreService } from '@jurca-raul/core-auth';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'jurca-raul-root',
  template: ' <router-outlet/> ',
  standalone: true,
  imports: [RouterOutlet],
})
export class RootComponent {}

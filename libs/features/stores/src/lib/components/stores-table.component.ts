import { Component, DestroyRef, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgFor, NgIf } from '@angular/common';
import { JrDatePipe } from '@jurca-raul/pipes/date';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ButtonComponent } from '@jurca-raul/components/button';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { IconButtonComponent } from '@jurca-raul/components/icon-button';
import { IconComponent } from '@jurca-raul/components/icon';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { NgArrayPipesModule } from 'ngx-pipes';
import { InputDirective, InputGroupComponent, SuffixDirective } from '@jurca-raul/components/form';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { TablePreloaderComponent } from '@jurca-raul/components/table-preloader';
import { Stores } from '@jurca-raul/domains/stores';
import { StoreService } from '@jurca-raul/services/store';
import { AddEditStoreModalComponent } from '@jurca-raul/modals/add-edit-view-stores';

@Component({
  selector: 'jr-stores',
  templateUrl: './stores-table.component.html',
  standalone: true,
  imports: [
    NgIf,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NgFor,
    JrDatePipe,
    IconComponent,
    TooltipModule,
    IconButtonComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    SuffixDirective,
    IconComponent,
    NgArrayPipesModule,
    InputGroupComponent,
    TablePreloaderComponent,
  ],
})
export class StoresComponent implements OnInit {
  readonly noPermissonsMessage = 'You do not have permissions to perform this action';

  stores: Stores[] = [];

  searchTerm?: string;

  countedRows?: number;
  isLoading: boolean = false;

  sortType: string = '';
  sortReverse: boolean = false;

  permissions: {
    createEntries?: boolean;
    removeEntries?: boolean;
  } = { createEntries: false, removeEntries: false };

  selectstores: any[] = [];

  p: number = 1;
  user: any;

  constructor(
    private destroyRef: DestroyRef,
    private storeService: StoreService,
    private bsModalService: BsModalService,
    public bsModalRef: BsModalRef,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService // used to get user information
  ) {
    this.loadPermissions();
  }

  ngOnInit(): void {
    this.countRows();
    this.fetchData();
  }

  private fetchData() {
    this.isLoading = true;
    setTimeout(() => {
      this.storeService.getStores().then((storesDB) => {
        this.stores = storesDB;
        this.isLoading = false;
      });
    }, 2000);
  }

  async countRows() {
    this.storeService.getStores().then((storesDB) => {
      this.countedRows = storesDB.length;
    });
  }

  private reloadTable() {
    this.countRows();
    this.stores = [];
    this.fetchData();
  }

  private loadPermissions() {
    this.authCoreService.getPermissions().subscribe((permissions) => {
      const permissionsObject: Record<string, boolean> = {};
      permissions.forEach((permissions) => {
        const permissionName = permissions.permission_name;
        permissionsObject[permissionName] = true;
      });
      this.permissions = permissionsObject;
    });
  }

  sort(sortColumn: string) {
    if (this.sortType === sortColumn) {
      this.sortReverse = !this.sortReverse;
    } else {
      this.sortType = sortColumn;
      this.sortReverse = false;
    }

    this.stores.sort((a: any, b: any) => {
      if (a[this.sortType] < b[this.sortType]) {
        return this.sortReverse ? 1 : -1;
      }
      if (a[this.sortType] > b[this.sortType]) {
        return this.sortReverse ? -1 : 1;
      }
      return 0;
    });
  }

  addStore() {
    this.bsModalRef = this.bsModalService.show(AddEditStoreModalComponent, { class: 'modal-xl' });
    const modal: AddEditStoreModalComponent = this.bsModalRef.content;
    modal.tableRefresh.subscribe(() => {
      this.fetchData();
    });
  }

  openStoreDetails(store: Stores) {
    this.bsModalRef = this.bsModalService.show(AddEditStoreModalComponent, {
      class: 'modal-xl',
      initialState: { viewDetails: true, store: store },
    });
  }

  editStore(store: Stores) {
    this.bsModalRef = this.bsModalService.show(AddEditStoreModalComponent, {
      class: 'modal-xl',
      initialState: { store: store },
    });
    const modal: AddEditStoreModalComponent = this.bsModalRef.content;
    modal.tableRefresh.subscribe(() => {
      this.fetchData();
    });
  }

  deleteStore(store: Stores) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: { action: 'Delete' },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.authCoreService
        .getUserInfo()
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (user: any) => {
            store.deleted_at = new Date().toISOString();
            store.deleted_by_id = user.id;
            store.deleted_by_user = user.name;

            this.storeService.moveStoreToArchive(store).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Store not deleted');
              } else {
                this.storeService.deleteStore(store.id).then((res) => {
                  if (res.error) {
                    this.toastr.error(`${res.error?.details}`, 'Store not deleted');
                  } else {
                    this.toastr.info('This action can be reverted. For more information contact your manager', 'Info!');
                    this.toastr.success('Store deleted successfully', 'Success!');
                  }

                  this.reloadTable();
                  this.bsModalRef.hide();
                });
                this.toastr.success('Store deleted successfully', 'Success!');
              }
            });
          },
        });
    });
    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }
}

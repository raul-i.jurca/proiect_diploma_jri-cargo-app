import { LowerCasePipe } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ButtonComponent } from '@jurca-raul/components/button';
import { ModalComponent } from '@jurca-raul/modals/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: './confirm-modal.component.html',
  standalone: true,
  imports: [LowerCasePipe, ModalComponent, ButtonComponent],
})
export class ConfirmModalComponent {
  @Input() message?: string;
  @Input() action?: string;

  @Input() param1?: string;
  @Input() param2?: string;

  @Output() Confirm: EventEmitter<any> = new EventEmitter();
  @Output() Decline: EventEmitter<any> = new EventEmitter();

  constructor(public bsModalRef: BsModalRef) {}

  confirm(): void {
    this.Confirm.emit();
  }

  decline(): void {
    this.Decline.emit();
  }
}

const express = require('express');
const app = express();
const { auth } = require('express-oauth2-jwt-bearer');

const port = process.env.PORT || 8080;

const jwtCheck = auth({
  audience: 'api/cargo-app',
  issuerBaseURL: 'https://rauljurca.eu.auth0.com/',
  tokenSigningAlg: 'RS256',
});

// enforce on all endpoints
app.use(jwtCheck);

app.get('/authorized', function (req, res) {
  res.send('Secured Resource');
});

app.listen(port);

console.log(`Server is listening at http://localhost:${port}`);

var myHeaders = new Headers();
myHeaders.append('Accept', 'application/json');
myHeaders.append('Authorization', 'Bearer 🔒');

var requestOptions = {
  method: 'GET',
  headers: myHeaders,
  redirect: 'follow',
};

fetch('https://rauljurca.eu.auth0.com/api/v2/users/auth0%7C6460d25a0cc35e7571273c6b/permissions', requestOptions)
  .then((response) => response.text())
  .then((result) => console.log(result))
  .catch((error) => console.log('error', error));

import { Component, DestroyRef, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { DatePipe, NgFor, NgIf } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { ToastrService } from 'ngx-toastr';
import { ModalComponent } from '@jurca-raul/modals/modal';
import { ButtonComponent } from '@jurca-raul/components/button';
import { InputDirective, InputGroupComponent } from '@jurca-raul/components/form';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { DropdownComponent } from '@jurca-raul/components/dropdown';
// import { jsPDF } from 'jspdf';
import { Stores } from '@jurca-raul/domains/stores';
import { StoreService } from '@jurca-raul/services/store';
import jsPDF from 'jspdf';

@Component({
  templateUrl: './add-edit-view-stores-modal.component.html',
  styleUrls: ['./add-edit-view-stores-modal.component.scss'],
  standalone: true,
  providers: [DatePipe],
  imports: [
    NgIf,
    NgFor,
    ReactiveFormsModule,
    FormsModule,
    ModalComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    BsDatepickerModule,
    DropdownComponent,
  ],
})
export class AddEditStoreModalComponent {
  @Output() tableRefresh: EventEmitter<any> = new EventEmitter();

  newStoreForm: FormGroup = new FormGroup({});

  viewDetails: boolean = false;

  stores: Stores[] | null = [];
  store!: Stores;

  user: any;

  // datepicker configurations
  bsDateConfig: Partial<BsDatepickerModule> = {
    dateInputFormat: 'DD/MM/YYYY',
    containerClass: 'theme-dark-blue',
    isAnimated: true,
    adaptivePosition: true,
  };
  minDateCheckIn?: Date;

  modalRef?: BsModalRef;
  message?: string;

  constructor(
    private destroyRef: DestroyRef,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private modalService: BsModalService,
    public bsModalRef: BsModalRef,
    private storeService: StoreService,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.newStoreForm = this.formBuilder.group({
      store_name: new FormControl(
        { value: this.store ? this.store.store_name : null, disabled: this.viewDetails },
        Validators.required
      ),
      store_phone: new FormControl(
        { value: this.store ? this.store.store_phone : null, disabled: this.viewDetails },
        Validators.required
      ),
      store_email: new FormControl(
        { value: this.store ? this.store.store_email : null, disabled: this.viewDetails },
        Validators.required
      ),

      store_address_county: new FormControl(
        { value: this.store ? this.store.store_address_county : null, disabled: this.viewDetails },
        Validators.required
      ),
      store_address_city: new FormControl(
        { value: this.store ? this.store.store_address_city : null, disabled: this.viewDetails },
        Validators.required
      ),
      store_address_street: new FormControl(
        { value: this.store ? this.store.store_address_street : null, disabled: this.viewDetails },
        Validators.required
      ),
      store_address_street_no: new FormControl(
        { value: this.store ? this.store.store_address_street_no : null, disabled: this.viewDetails },
        Validators.required
      ),

      contact_name: new FormControl(
        { value: this.store ? this.store.contact_name : null, disabled: this.viewDetails },
        Validators.required
      ),
      contact_phone: new FormControl(
        { value: this.store ? this.store.contact_phone : null, disabled: this.viewDetails },
        Validators.required
      ),
      contact_email: new FormControl(
        { value: this.store ? this.store.contact_email : null, disabled: this.viewDetails },
        Validators.required
      ),

      comments: new FormControl({ value: this.store ? this.store.comments : null, disabled: this.viewDetails }),
    });
  }

  onSave(newStoreForm: FormGroup): void {
    this.modalRef = this.modalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
    });
    this.modalRef.content.Confirm.subscribe(() => {
      this.saveNewDriver(newStoreForm);
      this.modalRef?.hide();
    });
    this.modalRef.content.Decline.subscribe(() => {
      this.modalRef?.hide();
    });
  }

  saveNewDriver(newStoreForm: FormGroup) {
    this.authCoreService
      .getUserInfo()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (user: any) => {
          if (!this.store) {
            if (newStoreForm.valid) {
              const store = {
                created_at: new Date().toISOString(),
                created_by_id: user.id,
                created_by_user: user.name,

                store_name: newStoreForm.value.store_name,
                store_phone: newStoreForm.value.store_phone,
                store_email: newStoreForm.value.store_email,

                store_address_county: newStoreForm.value.store_address_county,
                store_address_city: newStoreForm.value.store_address_city,
                store_address_street: newStoreForm.value.store_address_street,
                store_address_street_no: newStoreForm.value.store_address_street_no,

                contact_name: newStoreForm.value.contact_name,
                contact_phone: newStoreForm.value.contact_phone,
                contact_email: newStoreForm.value.contact_email,

                comments: newStoreForm.value.comments,
              };
              this.storeService.newsStore(store).then((res) => {
                if (res.error) {
                  this.toastr.error(`${res.error?.details}`, 'Store not added');
                } else {
                  this.toastr.success('Store added successfully', 'Success!');
                  this.tableRefresh.emit();
                }
              });
              this.bsModalRef.hide();
            } else {
              this.toastr.error('Please fill in all the fields', 'Error!');
            }
          } else {
            const store = {
              updated_at: new Date().toISOString(),
              updated_by_id: user.id,
              updated_by_user: user.name,

              store_name: newStoreForm.value.store_name,
              store_phone: newStoreForm.value.store_phone,
              store_email: newStoreForm.value.store_email,

              store_address_county: newStoreForm.value.store_address_county,
              store_address_city: newStoreForm.value.store_address_city,
              store_address_street: newStoreForm.value.store_address_street,
              store_address_street_no: newStoreForm.value.store_address_street_no,

              contact_name: newStoreForm.value.contact_name,
              contact_phone: newStoreForm.value.contact_phone,
              contact_email: newStoreForm.value.contact_email,

              comments: newStoreForm.value.comments,
            };
            this.storeService.updateStore(store, this.store.id).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Store not updated');
              } else {
                this.toastr.success('Store updated successfully', 'Success!');

                this.tableRefresh.emit();
                this.bsModalRef.hide();
              }
            });
          }
        },
      });
  }

  print(store: Stores) {
    const doc = new jsPDF();

    doc.text('Store Info', 10, 10);
    doc.text('Store ID: ' + store.id, 10, 20);
    doc.text('Store Name: ' + store.store_name, 10, 30);
    doc.text('Store Phone: ' + store.store_phone, 10, 40);
    doc.text('Store Email: ' + store.store_email, 10, 50);
    doc.text('Store Address County: ' + store.store_address_county, 10, 60);
    doc.text('Store Address City: ' + store.store_address_city, 10, 70);
    doc.text('Store Address Street: ' + store.store_address_street, 10, 80);
    doc.text('Store Address Street No: ' + store.store_address_street_no, 10, 90);
    doc.text('Contact Name: ' + store.contact_name, 10, 100);
    doc.text('Contact Phone: ' + store.contact_phone, 10, 110);
    doc.text('Contact Email: ' + store.contact_email, 10, 120);
    doc.text('Comments: ' + store.comments, 10, 130);
    doc.text('Created At: ' + store.created_at, 10, 140);
    doc.text('Created By User with ID: ' + store.created_by_id, 10, 150);
    doc.text('Created By User: ' + store.created_by_user, 10, 160);

    doc.save(`${store.id}_${store.store_name}-profile.pdf`);
  }
}

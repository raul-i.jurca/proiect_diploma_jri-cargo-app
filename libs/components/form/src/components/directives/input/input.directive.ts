/* eslint-disable no-extra-boolean-cast */
import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  Inject,
  OnInit,
  Optional,
  Self,
} from '@angular/core';
import { NgControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { InputTypes } from '../../input-group/input.models';

@Directive({
  selector: 'input[JrInput], select[JrInput], textarea[JrInput]',
  exportAs: 'jrInput',
  standalone: true,
})
export class InputDirective implements OnInit {
  isFocused = false;
  nativeElementValueChanges!: BehaviorSubject<string>;

  @HostBinding('class') hostClass = 'jr-form-field-input';

  @HostListener('focus', ['$event']) onFocus() {
    this.isFocused = true;
  }

  @HostListener('blur', ['$event']) onBlur() {
    this.isFocused = false;
  }

  @HostListener('input', ['$event'])
  @HostListener('change', ['$event'])
  onChange(event: { target: { value: string } }) {
    this.nativeElementValueChanges.next(event.target.value);
  }

  constructor(
    @Inject(ElementRef) public elementRef: ElementRef,
    @Inject(NgControl) @Optional() @Self() public ngControl: NgControl
  ) {}

  ngOnInit() {
    this.nativeElementValueChanges = new BehaviorSubject<string>(
      this.ngControl?.control?.value || this.elementRef.nativeElement.value
    );
  }

  get hasError(): boolean {
    if (!this.ngControl?.control) {
      return false;
    }
    return this.ngControl.control.touched && this.ngControl.control.invalid;
  }

  get inputType(): string {
    return this.elementRef.nativeElement.type;
  }

  clearInput(): void {
    if (!!this.ngControl?.control) {
      this.ngControl?.control.setValue('');
    } else {
      this.elementRef.nativeElement.value = '';
    }

    this.nativeElementValueChanges.next('');
  }

  togglePasswordVisibility(): void {
    if (!this.inputType || this.inputType === InputTypes.Email) {
      return;
    }

    if (this.inputType === InputTypes.Text) {
      this.elementRef.nativeElement.type = InputTypes.Password;
      return;
    }

    this.elementRef.nativeElement.type = InputTypes.Text;
  }
}

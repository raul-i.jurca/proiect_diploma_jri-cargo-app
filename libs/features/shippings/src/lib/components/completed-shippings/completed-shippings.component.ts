import { NgIf, NgFor } from '@angular/common';
import { Component, DestroyRef } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ButtonComponent } from '@jurca-raul/components/button';
import { InputGroupComponent, InputDirective, SuffixDirective } from '@jurca-raul/components/form';
import { IconComponent } from '@jurca-raul/components/icon';
import { IconButtonComponent } from '@jurca-raul/components/icon-button';
import { TablePreloaderComponent } from '@jurca-raul/components/table-preloader';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { Driver } from '@jurca-raul/domains/driver';
import { AddEditShippingModalComponent } from '@jurca-raul/modals/add-edit-view-shipping';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { JrDatePipe } from '@jurca-raul/pipes/date';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgArrayPipesModule } from 'ngx-pipes';
import { ToastrService } from 'ngx-toastr';
import { ShippingsHeaderComponent, ShippingsTabEnum } from '../shippings-header';
import { ShippingsService } from '@jurca-raul/services/shippings';
import { Shipping } from '@jurca-raul/domains/shipping';
import { forkJoin } from 'rxjs';
import { Vehicle } from '@jurca-raul/domains/vehicle';

@Component({
  selector: 'jr-active-shippings',
  templateUrl: './completed-shippings.component.html',
  standalone: true,
  imports: [
    NgIf,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NgFor,
    JrDatePipe,
    IconComponent,
    TooltipModule,
    IconButtonComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    SuffixDirective,
    IconComponent,
    NgArrayPipesModule,
    InputGroupComponent,
    TablePreloaderComponent,
    ShippingsHeaderComponent,
  ],
})
export class CompletedShippingsComponent {
  readonly noPermissonsMessage = 'You do not have permissions to perform this action';

  shippingsTabEnum = ShippingsTabEnum;

  activeShippings: Shipping[] = [];

  searchTerm?: string;
  sortType: string = '';
  sortReverse: boolean = false;

  countedRows?: number;
  isLoading: boolean = false;

  permissions: {
    createDelivery?: boolean;
    removeEntries?: boolean;
  } = { createDelivery: false, removeEntries: false };

  selectDrivers: any[] = [];

  p: number = 1;
  user: any;

  storeDetails: any;
  assignedDriver?: Driver[] | null;
  assignedVehicle?: Vehicle[] | null;

  constructor(
    private destroyRef: DestroyRef,
    private authCoreService: AuthCoreService,
    private shippingsService: ShippingsService,
    private bsModalService: BsModalService,
    public bsModalRef: BsModalRef,
    private toastr: ToastrService
  ) {
    this.loadPermissions();
  }

  ngOnInit(): void {
    this.countRows();
    this.fetchData();
  }

  private loadPermissions() {
    this.authCoreService.getPermissions().subscribe((permissions) => {
      const permissionsObject: Record<string, boolean> = {};
      permissions.forEach((permissions) => {
        const permissionName = permissions.permission_name;
        permissionsObject[permissionName] = true;
      });
      this.permissions = permissionsObject;
    });
  }

  private fetchData() {
    this.activeShippings = [];
    this.isLoading = true;
    this.countRows();

    setTimeout(() => {
      this.shippingsService.getCompletedShippings().then((completedShippingsRespone) => {
        this.countedRows = completedShippingsRespone?.data ? completedShippingsRespone.data.length : 0;

        this.activeShippings = completedShippingsRespone?.data ? completedShippingsRespone.data : [];

        this.getStoreDetails();
        this.getAssignedDriverDetails();
        this.getAssignedVehicleDetails();
      });
      this.isLoading = false;
    }, 2000);
  }

  private countRows() {
    this.shippingsService.getCompletedShippings().then((completedShippingsRespone) => {
      this.countedRows = completedShippingsRespone?.data ? completedShippingsRespone.data.length : 0;
    });
  }

  sort(sortColumn: string) {
    if (this.sortType === sortColumn) {
      this.sortReverse = !this.sortReverse;
    } else {
      this.sortType = sortColumn;
      this.sortReverse = false;
    }

    this.activeShippings.sort((a: any, b: any) => {
      if (a[this.sortType] < b[this.sortType]) {
        return this.sortReverse ? 1 : -1;
      }
      if (a[this.sortType] > b[this.sortType]) {
        return this.sortReverse ? -1 : 1;
      }
      return 0;
    });
  }

  private getStoreDetails() {
    // here we are creating an array of requests to get client data for each active shipping
    const clientRequests = this.activeShippings.map((shipping) =>
      this.shippingsService.getClientsById(shipping.store_id)
    );

    // here we are using forkJoin to get all client data at once
    forkJoin(clientRequests)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (clientsData) => {
          this.activeShippings.forEach((shipping, index) => {
            if (clientsData[index]?.data?.length > 0) {
              shipping.storeDetails = clientsData[index].data[0];
            }
          });

          // At this point, each shipping object in this.activeShippings has an assignedClient property containing the client data
          // You can now use this data to render in your table
        },
      });
  }

  private getAssignedDriverDetails() {
    const driverRequests = this.activeShippings.map((shipping) =>
      this.shippingsService.getDriversById(shipping.driver_id)
    );
    forkJoin(driverRequests)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (driversData) => {
          this.activeShippings.forEach((shipping, index) => {
            if (driversData[index]?.data?.length > 0) {
              shipping.assignedDriver = driversData[index].data[0];
            }
          });
        },
      });
  }

  private getAssignedVehicleDetails() {
    const vehicleRequests = this.activeShippings.map((shipping) =>
      this.shippingsService.getVehiclesById(shipping.vehicle_id)
    );
    forkJoin(vehicleRequests)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (vehiclesData) => {
          this.activeShippings.forEach((shipping, index) => {
            if (vehiclesData[index]?.data?.length > 0) {
              shipping.assignedVehicle = vehiclesData[index].data[0];
            }
            console.log(shipping.assignedVehicle);
          });
        },
      });
  }

  newDelivery() {
    this.bsModalRef = this.bsModalService.show(AddEditShippingModalComponent, { class: 'modal-md' });
    const modal: AddEditShippingModalComponent = this.bsModalRef.content;
    modal.tableRefresh.pipe(takeUntilDestroyed(this.destroyRef)).subscribe({
      next: () => {
        this.fetchData();
      },
    });
  }

  openShippingDetails(selectedShipping: any) {
    this.bsModalRef = this.bsModalService.show(AddEditShippingModalComponent, {
      class: 'modal-xl',
      initialState: { viewDetails: true, shipping: selectedShipping },
    });
  }

  // This action can be made only by the manager
  markAsDelivered(selectedShipping: Shipping) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: { action: 'Mark as Active' },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      selectedShipping.completed_at;
      selectedShipping.completed_by_id = '';
      selectedShipping.completed_by_user = '';
      selectedShipping.is_completed = false;

      Promise.all([
        this.shippingsService.markVehiclesOnNewShipping(selectedShipping.vehicle_id, true),
        this.shippingsService.markStoreOnNewShipping(selectedShipping.store_id, true),
        this.shippingsService.markDriverOnNewShipping(selectedShipping.driver_id, true),
      ]).then((responses) => {
        const errorResponse = responses.find((res) => res.error);
        if (errorResponse) {
          this.toastr.error(`${errorResponse.error?.details}`, 'Shipping not market as completed');
        } else {
          this.shippingsService.updateshippings(selectedShipping, selectedShipping.id).then((res) => {
            if (res.error) {
              this.toastr.error(`${res.error?.details}`, 'Shipping not market as completed');
            } else {
              this.toastr.success('Shipping was marked as completed', 'Success!');
            }

            this.fetchData();

            this.bsModalRef.hide();
          });
        }
      });
      this.bsModalRef.content.Decline.subscribe(() => {
        this.bsModalRef.hide();
        this.toastr.info('Action canceled', 'Info!');
      });
    });
  }

  editShipping(selectedShipping: Shipping) {
    this.bsModalRef = this.bsModalService.show(AddEditShippingModalComponent, {
      class: 'modal-md',
      initialState: { shipping: selectedShipping },
    });
    const modal: AddEditShippingModalComponent = this.bsModalRef.content;
    modal.tableRefresh.subscribe(() => {
      this.fetchData();
    });
  }

  deleteShipping(selectedShipping: Shipping) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: {
        action: 'Delete',
        message: '!!! ATENTION this action is NOT REVERSIBLE. ALL THE DATA WILL BE LOST FOREVER!',
      },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.shippingsService.removeShippingFromArchive(selectedShipping.id).then((res) => {
        if (res.error) {
          this.toastr.error(`${res.error?.details}`, 'Shipping not deleted');
        } else {
          this.toastr.success('Shipping deleted', 'Success!');
          this.bsModalRef.hide();
        }
        this.fetchData();
      });
    });

    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }
}

import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[JrPrefix]',
  standalone: true,
})
export class PrefixDirective {
  @HostBinding('class') hostClass = 'jr-form-field-prefix';
}

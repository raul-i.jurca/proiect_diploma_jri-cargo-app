/* eslint-disable @typescript-eslint/prefer-as-const */
import { NgFor } from '@angular/common';
import {
  Component,
  HostBinding,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { PrefixDirective, SuffixDirective } from '../directives';

@Component({
  selector: 'jr-form-label',
  templateUrl: './form-label.component.html',
  styleUrls: ['./form-label.component.scss'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [NgFor, PrefixDirective, SuffixDirective],
})
export class FormLabelComponent {
  @HostBinding('class.jr-form-label-component') containerClass = true;
  @Input() name: string = '';
  @Input() focused: false = false;
  @Input() readonly: false = false;
}

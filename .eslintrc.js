module.exports = {
  root: true,
  overrides: [
    {
      files: ['*.ts'],
      extends: [
        'plugin:@nrwl/nx/angular',
        'plugin:@angular-eslint/template/process-inline-templates',
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:import/typescript',
      ],
      plugins: ['@nx/eslint-plugin', 'import', '@typescript-eslint'],
      rules: {
        '@angular-eslint/directive-selector': [
          'error',
          {
            type: 'attribute',
            prefix: 'Jr',
            style: 'camelCase',
          },
        ],
        '@angular-eslint/component-selector': [
          'error',
          {
            type: 'element',
            prefix: 'jr',
            style: 'kebab-case',
          },
        ],
        '@typescript-eslint/naming-convention': [
          'error',
          {
            selector: 'enum',
            format: ['StrictPascalCase'],
            suffix: ['Enum'],
          },
        ],
        '@typescript-eslint/no-unused-vars': ['warn', { ignoreRestSiblings: true }],
        '@typescript-eslint/no-inferrable-types': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@angular-eslint/use-lifecycle-interface': 'off',
        '@typescript-eslint/prefer-as-const': 'off',
      },
    },
    {
      files: ['*.html'],
      extends: ['plugin:@nrwl/nx/angular-template'],
      rules: {},
    },
  ],
};

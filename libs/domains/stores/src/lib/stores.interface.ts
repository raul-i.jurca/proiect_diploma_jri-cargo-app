export interface Stores {
  id: string;

  shippings: string[]; //TODO- check if needed?

  store_name: string;

  store_phone: string;
  store_email: string;

  store_address_county: string;
  store_address_city: string;
  store_address_street: string;
  store_address_street_no: string;

  contact_name: string;
  contact_phone: string;
  contact_email: string;

  comments: string;

  created_at: string;
  created_by_id: string;
  created_by_user: string;

  updated_at: string;
  updated_by_id: string;
  updated_by_user: string;

  deleted_at: string;
  deleted_by_id: string;
  deleted_by_user: string;

  restored_at: string;
  restored_by_id: string;
  restored_by_user: string;
}

import { Routes } from '@angular/router';
import { StoresComponent } from './components/stores-table.component';

export const routes: Routes = [{ path: '', component: StoresComponent, data: { title: 'Stores' } }];
export default routes;

import { Injectable } from '@angular/core';
import { supabaseDb } from '@jurca-raul/environment';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  getStores() {
    return supabaseDb
      .from('stores')
      .select('*')
      .then((res: any) => res.data);
  }

  getStoresFromArchive() {
    return supabaseDb
      .from('archive_stores')
      .select('*')
      .then((res: any) => res.data);
  }

  // methods for drivers table
  newsStore(store: any) {
    return supabaseDb.from('stores').insert([store]).select();
  }

  updateStore(store: any, id: string) {
    return supabaseDb.from('stores').update(store).eq('id', id).select();
  }

  deleteStore(id: string) {
    return supabaseDb.from('stores').delete().eq('id', id).select();
  }

  moveStoreToArchive(store: any) {
    return supabaseDb.from('archive_stores').insert([store]).select();
  }

  removeStoreFromArchive(id: string) {
    return supabaseDb.from('archive_stores').delete().eq('id', id).select();
  }
}

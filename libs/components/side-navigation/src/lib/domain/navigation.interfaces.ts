import { RouterLinkService } from '@jurca-raul/services/router';

export interface NavigationItem {
  name: string;
  routerLink: string[];
  class?: string;
  icon?: NavigationItemEnum;
}

enum NavigationItemEnum {
  ID_CARD = 'fa-solid fa-id-card',
  TRUCK_FAST = 'fa-solid fa-truck-fast',
  TRUCK = 'fa-solid fa-truck',
  HANDSHAKE = 'fa-solid fa-handshake',
  SETTINGS = 'fa-solid fa-gear',
  HISTORY = 'fa-solid fa-history',
  TOOLS = 'fa-solid fa-screwdriver-wrench',
}

export const SIDE_NAVIGATION_ITEMS = (routerLinkService: RouterLinkService): NavigationItem[] => [
  {
    name: 'Drivers',
    routerLink: routerLinkService.routerLink.drivers,
    icon: NavigationItemEnum.ID_CARD,
  },

  {
    name: 'Deliveries',
    routerLink: routerLinkService.routerLink.deliveries,
    icon: NavigationItemEnum.TRUCK_FAST,
  },
  {
    name: 'Vehicles',
    routerLink: routerLinkService.routerLink.vehicles,
    icon: NavigationItemEnum.TRUCK,
  },
  {
    name: 'Stores',
    routerLink: routerLinkService.routerLink.stores,
    icon: NavigationItemEnum.HANDSHAKE,
  },
  {
    name: 'Archive',
    routerLink: routerLinkService.routerLink.archive,
    icon: NavigationItemEnum.HISTORY,
  },
  // {
  //   name: 'Administration',
  //   routerLink: routerLinkService.routerLink.administration,
  //   icon: NavigationItemEnum.TOOLS,
  // },
];

# JRI CARGO APP

<a alt="Nx logo" href="https://jri-cargo-app.vercel.app" target="_blank" rel="noreferrer"><img src="app/assets/images/jri-market-logistic-high-resolution-logo.svg" width="500"></a>

✨ ** JRI CARGO APP ** ✨

## Acces online la aplicatie

Accesati:
https://jri-cargo-app.vercel.app

Utilizatori ai aplicatiei (fiecare utilizator are permisiuni diferite):
jurcaraull+dataoperator@gmail.com
jurcaraull+unverified@gmail.com (user that needs to verify the email adress)
jurcaraull+manager@gmail.com
jurcaraull+planner@gmail.com

Parola tuturor utilizatorilor
password (litere mici)

## Instalarea aplicatiei local
   1.In terminal rulati:
`git clone https://gitlab.upt.ro/raul-i.jurca/jri-cargo-app.git`

   2. run :
     `npm install`


## Pornirea aplicatiei local

    1. Rulati in terminal
         `npm run develop` 
    2. Accesati 
     http://localhost:4200/

    3. Folositi utilizatori enumerati mai sus

## Arhitectura aplicatiei

    Pentru a intelege arhitectura aplicatiei puteti genera graficul aplicatiei ruland:
     `nx graph`

------------------------------------------------------------------------------------------------------

##ENGLISH

## Online Access to the Aplication

This project can be also accesed from this url:
https://jri-cargo-app.vercel.app

In order to acces the App, use one of the above user
     *note: each user has different permissions, so for some user a part of the content will not be accesible

jurcaraull+dataoperator@gmail.com
jurcaraull+unverified@gmail.com (user that needs to verify the email adress)
jurcaraull+manager@gmail.com
jurcaraull+planner@gmail.com

The password for all user is <b>password</b> (small caps)
     *note: the password can be configured to inclide special characters, uppercase, symbols or even 2FA/MFA (two factor authentication/multi factor autentication). 
     *note: IT WAS SET TO PASSWORD IN ORDER TO BE ABLE TO PRESENT THE PROJECT EASLY OR TO GRANT ACCESS FOR THE REVIEWERS

# Start Aplication on local machine

## Clone repositori

    1. In your terminal paste:
     `git clone https://gitlab.upt.ro/raul-i.jurca/jri-cargo-app.git`
    2. After clonong the repo navigate to the aplication folder or open your IDE -> right click on the main folder of the app-> open in terminal (the terminal shoul open at the bottom of the IDE (VS CODE)
    3. run :
     `npm install`

## Start local development server

     Run:
        `npm run develop` 
     to start the app.
     Navigate to http://localhost:4200/. 
     The app will automatically reload if you change any of the source files.


## Understand this workspace

     In order to unserstand the architecture of the app run `nx graph` to see a diagram of the dependencies of the projects.

## If any error occurs

     1.make sure node modules where installed (`npm install`)
     2. run 
     `npx nx reset`
     3. restart development server
     `npm run develop`

## Remote caching

Run `npx nx connect-to-nx-cloud` to enable [remote caching](https://nx.app) and make CI faster.

## Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.


## Generate new library

Run `nx g lib libName --directory=libs/folderName/folderName`




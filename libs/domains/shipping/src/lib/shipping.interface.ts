import { Stores } from '@jurca-raul/domains/stores';
import { Driver } from '@jurca-raul/domains/driver';
import { Vehicle } from '@jurca-raul/domains/vehicle';

export interface Shipping {
  id: string;

  driver_id: string;
  store_id: string;
  vehicle_id: string;

  created_at: string;
  created_by_id: string;
  created_by_user: string;

  updated_at: string;
  updated_by_id: string;
  updated_by_user: string;

  deleted_at: string;
  deleted_by_id: string;
  deleted_by_user: string;

  restored_at: string;
  restored_by_id: string;
  restored_by_user: string;

  is_completed: boolean;
  completed_at: string;
  completed_by_id: string;
  completed_by_user: string;

  storeDetails: Stores;
  assignedDriver: Driver;
  assignedVehicle: Vehicle;
}

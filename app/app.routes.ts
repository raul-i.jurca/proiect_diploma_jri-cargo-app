import { Routes } from '@angular/router';
import { AuthGuard } from '@auth0/auth0-angular';
import { MainComponent } from '@jurca-raul/core';

export const APP_ROUTES: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'deliveries',
        pathMatch: 'full',
      },
      {
        path: 'drivers',
        loadChildren: () => import('@jurca-raul/features/drivers'),
      },
      {
        path: 'vehicles',
        loadChildren: () => import('@jurca-raul/features/vehicles'),
      },
      {
        path: 'deliveries',
        loadChildren: () => import('@jurca-raul/features/shippings'),
      },
      {
        path: 'archive',
        loadChildren: () => import('@jurca-raul/features/archive'),
      },
      {
        path: 'stores',
        loadChildren: () => import('@jurca-raul/features/stores'),
      },
      {
        path: 'administration',
        loadChildren: () => import('@jurca-raul/features/administration'),
      },
    ],
  },
  {
    path: '**',
    redirectTo: 'deliveries',
    pathMatch: 'full',
  },
];

import { NgClass, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  Icon,
  IconAnimation,
  IconAnimations,
  IconSetEnum,
  IconSize,
  IconSizes,
  IconStyle,
} from '../domain';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'jr-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgIf, NgClass],
})
export class IconComponent {
  @Input() icon: Icon = 'UNSET';
  @Input() iconAnimation: IconAnimation = 'UNSET';
  @Input() iconStyle: IconStyle = 'ICON';
  @Input() color: string = 'red';
  @Input() size: IconSize = 'MEDIUM';

  iconSizes = IconSizes;
  iconAnimations = IconAnimations;
  iconSetEnum = IconSetEnum;
}

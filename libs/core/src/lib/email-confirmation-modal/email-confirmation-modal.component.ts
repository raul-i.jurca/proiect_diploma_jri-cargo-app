import { Component, OnInit } from '@angular/core';
import { ModalComponent } from '@jurca-raul/modals/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Observable, interval } from 'rxjs';

@Component({
  templateUrl: './email-confirmation-modal.component.html',
  standalone: true,
  imports: [ModalComponent],
})
export class EmailConfirmationModalComponent implements OnInit {
  countdown: number = 5;
  remainingTime$: Observable<number>;

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit(): void {
    this.remainingTime$ = interval(1000);

    this.remainingTime$.subscribe(() => {
      if (this.countdown > 0) {
        this.countdown--;
      }
      if (this.countdown <= 0) {
        this.bsModalRef.hide();
      }
    });
  }
}

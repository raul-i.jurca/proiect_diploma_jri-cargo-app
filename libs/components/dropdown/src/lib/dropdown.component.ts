import { NgFor } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ButtonComponent } from '@jurca-raul/components/button';

export interface DropdownOption {
  optionValue: string;
  displayName: string;
}

@Component({
  selector: 'jr-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  standalone: true,
  imports: [NgFor, FormsModule, ButtonComponent],
})
export class DropdownComponent {
  @Input({ required: true }) label: any = 'Please select an option';
  @Input({ required: true }) options: DropdownOption[] = [];
  @Input() isDisabled: boolean = false;

  @Output() optionSelected: EventEmitter<DropdownOption> = new EventEmitter();

  activeOption!: DropdownOption;

  select(option: DropdownOption) {
    this.activeOption = option;
    this.optionSelected.emit(option);
  }
}

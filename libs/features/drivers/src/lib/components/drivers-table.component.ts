import { Component, DestroyRef, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgFor, NgIf } from '@angular/common';
import { JrDatePipe } from '@jurca-raul/pipes/date';
import { SupabaseService } from '@jurca-raul/services/supabase';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IconComponent } from '@jurca-raul/components/icon';
import { ButtonComponent } from '@jurca-raul/components/button';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { IconButtonComponent } from '@jurca-raul/components/icon-button';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { NgArrayPipesModule } from 'ngx-pipes';
import { InputDirective, InputGroupComponent, SuffixDirective } from '@jurca-raul/components/form';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Driver } from '@jurca-raul/domains/driver';
import { TablePreloaderComponent } from '@jurca-raul/components/table-preloader';
import { AddEditDriverModalComponent } from '@jurca-raul/modals/add-edit-view-driver';

export interface SortColumn {
  column: string;
  direction: string;
}

@Component({
  selector: 'jr-drivers',
  templateUrl: './drivers-table.component.html',
  standalone: true,
  imports: [
    NgIf,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NgFor,
    JrDatePipe,
    IconComponent,
    TooltipModule,
    IconButtonComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    SuffixDirective,
    IconComponent,
    NgArrayPipesModule,
    InputGroupComponent,
    TablePreloaderComponent,
    IconButtonComponent,
  ],
})
export class DriversComponent implements OnInit {
  readonly noPermissonsMessage = 'You do not have permissions to perform this action';
  drivers: Driver[] = [];
  driver!: Driver;
  searchTerm?: string;

  sortType: string = '';
  sortReverse: boolean = false;

  countedRows?: number;
  isLoading: boolean = false;

  permissions: {
    createEntries?: boolean;
    removeEntries?: boolean;
    createDeliveries?: boolean;
  } = { createEntries: false, removeEntries: false, createDeliveries: false };

  selectDrivers: any[] = [];

  p: number = 1;
  user: any;

  constructor(
    private destroyRef: DestroyRef,
    private supabaseService: SupabaseService,
    private bsModalService: BsModalService,
    public bsModalRef: BsModalRef,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService // used to get user information
  ) {
    this.loadPermissions();
  }

  ngOnInit(): void {
    this.countRows();
    this.fetchData();
    console.log(this.permissions.removeEntries);
  }

  private loadPermissions() {
    this.authCoreService.getPermissions().subscribe((permissions) => {
      const permissionsObject: Record<string, boolean> = {};
      permissions.forEach((permissions) => {
        const permissionName = permissions.permission_name;
        permissionsObject[permissionName] = true;
      });
      this.permissions = permissionsObject;
    });
  }

  sort(sortColumn: string) {
    if (this.sortType === sortColumn) {
      this.sortReverse = !this.sortReverse;
    } else {
      this.sortType = sortColumn;
      this.sortReverse = false;
    }

    this.drivers.sort((a: any, b: any) => {
      if (a[this.sortType] < b[this.sortType]) {
        return this.sortReverse ? 1 : -1;
      }
      if (a[this.sortType] > b[this.sortType]) {
        return this.sortReverse ? -1 : 1;
      }
      return 0;
    });
  }

  private fetchData() {
    this.isLoading = true;
    setTimeout(() => {
      this.supabaseService.getData('drivers').then((driversDB) => {
        this.drivers = driversDB;
        this.isLoading = false;
      });
    }, 2000);
  }

  async countRows() {
    this.supabaseService.getData('drivers').then((driversDB) => {
      this.countedRows = driversDB.length;
    });
  }

  private reloadTable() {
    this.drivers = [];
    this.fetchData();
  }

  addDriver() {
    this.bsModalRef = this.bsModalService.show(AddEditDriverModalComponent, { class: 'modal-xl' });
    const modal: AddEditDriverModalComponent = this.bsModalRef.content;
    modal.tableRefresh.subscribe(() => {
      this.fetchData();
    });
  }

  openDriverProfile(driver: Driver) {
    this.bsModalRef = this.bsModalService.show(AddEditDriverModalComponent, {
      class: 'modal-xl',
      initialState: { viewDetails: true, driver: driver },
    });
  }

  editDriver(driver: Driver) {
    this.bsModalRef = this.bsModalService.show(AddEditDriverModalComponent, {
      class: 'modal-xl',
      initialState: { driver: driver },
    });
    const modal: AddEditDriverModalComponent = this.bsModalRef.content;
    modal.tableRefresh.subscribe(() => {
      this.reloadTable();
    });
  }

  deleteDriver(driver: Driver) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: { action: 'Delete', param1: driver.s_name, param2: driver.f_name },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.authCoreService
        .getUserInfo()
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (user: any) => {
            driver.deleted_by_id = user.id;
            driver.deleted_by_name = user.name;
            driver.deleted_at = new Date().toISOString(); // this can be also handled by supabase

            this.supabaseService.moveDriverToArchive(driver).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Driver not deleted');
              } else {
                this.supabaseService.deleteDriver('drivers', driver.id).then((res) => {
                  if (res.error) {
                    this.toastr.error(`${res.error?.details}`, 'Driver not deleted');
                  } else {
                    this.toastr.info('This action can be reverted. For more information contact your manager', 'Info!');
                    this.toastr.success('Driver deleted successfully', 'Success!');
                  }

                  this.reloadTable();

                  this.bsModalRef.hide();
                });
                this.toastr.success('Driver deleted successfully', 'Success!');
                this.reloadTable();
                this.bsModalRef.hide();
              }
            });
          },
        });
    });
    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }
}

import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[JrSuffix]',
  standalone: true,
})
export class SuffixDirective {
  @HostBinding('class') hostClass = 'jr-form-field-suffix';
}

import { Component, DestroyRef, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgFor, NgIf } from '@angular/common';
import { JrDatePipe } from '@jurca-raul/pipes/date';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IconComponent } from '@jurca-raul/components/icon';
import { ButtonComponent } from '@jurca-raul/components/button';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { IconButtonComponent } from '@jurca-raul/components/icon-button';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { NgArrayPipesModule } from 'ngx-pipes';
import { InputDirective, InputGroupComponent, SuffixDirective } from '@jurca-raul/components/form';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { TablePreloaderComponent } from '@jurca-raul/components/table-preloader';
import { Vehicle } from '@jurca-raul/domains/vehicle';
import { AddEditVehicleModalComponent } from '@jurca-raul/modals/add-edit-view-vehicle';
import { VehiclesService } from '@jurca-raul/services/vehicle';

@Component({
  selector: 'jr-drivers',
  templateUrl: './vehicles.component.html',
  standalone: true,
  imports: [
    NgIf,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NgFor,
    JrDatePipe,
    IconComponent,
    TooltipModule,
    IconButtonComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    SuffixDirective,
    NgArrayPipesModule,
    InputGroupComponent,
    TablePreloaderComponent,
  ],
})
export class VehiclesComponent implements OnInit {
  readonly noPermissonsMessage = 'You do not have permissions to perform this action';

  vehicles: Vehicle[] = [];

  searchTerm?: string;

  countedRows?: number;
  isLoading: boolean = false;

  sortType: string = '';
  sortReverse: boolean = false;

  permissions: {
    createEntries?: boolean;
    removeEntries?: boolean;
  } = { createEntries: false, removeEntries: false };

  p: number = 1;
  user: any;

  constructor(
    private destroyRef: DestroyRef,
    private vehiclesService: VehiclesService,
    private bsModalService: BsModalService,
    public bsModalRef: BsModalRef,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService // used to get user information
  ) {
    this.loadPermissions();
  }

  ngOnInit(): void {
    this.countRows();
    this.fetchData();
  }

  private fetchData() {
    this.isLoading = true;
    setTimeout(() => {
      this.vehiclesService.getVehicles().then((vehiclesResponse) => {
        this.vehicles = vehiclesResponse;
        this.isLoading = false;
      });
    }, 2000);
  }

  async countRows() {
    this.vehiclesService.getVehicles().then((vehiclesResponse) => {
      this.countedRows = vehiclesResponse.length;
    });
  }

  private reloadTable() {
    this.vehicles = [];
    this.countRows();
    this.fetchData();
  }

  private loadPermissions() {
    this.authCoreService.getPermissions().subscribe((permissions) => {
      const permissionsObject: Record<string, boolean> = {};
      permissions.forEach((permissions) => {
        const permissionName = permissions.permission_name;
        permissionsObject[permissionName] = true;
      });
      this.permissions = permissionsObject;
    });
  }

  sort(sortColumn: string) {
    if (this.sortType === sortColumn) {
      this.sortReverse = !this.sortReverse;
    } else {
      this.sortType = sortColumn;
      this.sortReverse = false;
    }

    this.vehicles.sort((a: any, b: any) => {
      if (a[this.sortType] < b[this.sortType]) {
        return this.sortReverse ? 1 : -1;
      }
      if (a[this.sortType] > b[this.sortType]) {
        return this.sortReverse ? -1 : 1;
      }
      return 0;
    });
  }

  addVehicle() {
    this.bsModalRef = this.bsModalService.show(AddEditVehicleModalComponent, { class: 'modal-xl' });
    const modal: AddEditVehicleModalComponent = this.bsModalRef.content;
    modal.tableRefresh.subscribe(() => {
      this.fetchData();
    });
  }

  openVehicleProfile(vehicle: Vehicle) {
    this.bsModalRef = this.bsModalService.show(AddEditVehicleModalComponent, {
      class: 'modal-lg',
      initialState: { vehicle: vehicle, viewDetails: true },
    });
  }

  editVehicle(vehicle: Vehicle) {
    this.bsModalRef = this.bsModalService.show(AddEditVehicleModalComponent, {
      class: 'modal-xl',
      initialState: { vehicle: vehicle },
    });
    const modal: AddEditVehicleModalComponent = this.bsModalRef.content;
    modal.tableRefresh.subscribe(() => {
      this.reloadTable();
    });
  }

  deleteVehicle(vehicle: Vehicle) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: { action: 'Delete', param1: vehicle.registration_number },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.authCoreService
        .getUserInfo()
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (user: any) => {
            vehicle.deleted_by_id = user.id;
            vehicle.deleted_by_user = user.name;
            vehicle.deleted_at = new Date().toISOString(); // this can be also handled by supabase

            this.vehiclesService.moveVehicleToArchive(vehicle).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Vehicle not deleted');
              } else {
                this.vehiclesService.deleteVehicle(vehicle.id).then((res) => {
                  if (res.error) {
                    this.toastr.error(`${res.error?.details}`, 'Vehicle not deleted');
                  } else {
                    this.toastr.info('This action can be reverted. For more information contact your manager', 'Info!');
                    this.toastr.success('Vehicle deleted successfully', 'Success!');
                  }

                  this.reloadTable();

                  this.bsModalRef.hide();
                });
                this.toastr.success('Vehicle deleted successfully', 'Success!');
                this.reloadTable();

                this.bsModalRef.hide();
              }
            });
          },
        });
    });
    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }
}

export { IconComponent } from './lib/component/icon.component';
export {
  Icon,
  IconSize,
  IconAnimation,
  IconStyle,
} from './lib/domain/icon.constants';

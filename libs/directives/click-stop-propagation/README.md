# click-stop-propagation

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build click-stop-propagation` to build the library.

## Running unit tests

Run `nx test click-stop-propagation` to execute the unit tests via [Jest](https://jestjs.io).

import { NgIf, NgFor } from '@angular/common';
import { Component, DestroyRef } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ButtonComponent } from '@jurca-raul/components/button';
import { InputGroupComponent, InputDirective, SuffixDirective } from '@jurca-raul/components/form';
import { IconComponent } from '@jurca-raul/components/icon';
import { IconButtonComponent } from '@jurca-raul/components/icon-button';
import { TablePreloaderComponent } from '@jurca-raul/components/table-preloader';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { Driver } from '@jurca-raul/domains/driver';
import { AddEditShippingModalComponent } from '@jurca-raul/modals/add-edit-view-shipping';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { JrDatePipe } from '@jurca-raul/pipes/date';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgArrayPipesModule } from 'ngx-pipes';
import { ToastrService } from 'ngx-toastr';
import { ShippingsService } from '@jurca-raul/services/shippings';
import { Shipping } from '@jurca-raul/domains/shipping';
import { forkJoin } from 'rxjs';
import { Vehicle } from '@jurca-raul/domains/vehicle';
import { ArchiveHeaderComponent } from '../archive-header';
import { ArchiveTabEnum } from '../../domain';

@Component({
  selector: 'jr-active-shippings',
  templateUrl: './deliveries-archive.component.html',
  standalone: true,
  imports: [
    NgIf,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NgFor,
    JrDatePipe,
    IconComponent,
    TooltipModule,
    IconButtonComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    SuffixDirective,
    IconComponent,
    NgArrayPipesModule,
    InputGroupComponent,
    TablePreloaderComponent,
    ArchiveHeaderComponent,
  ],
})
export class DeliveriesArchiveComponent {
  readonly noPermissonsMessage = 'You do not have permissions to perform this action';

  archiveTabEnum = ArchiveTabEnum;

  shippings: Shipping[] = [];
  shipping!: Shipping;

  searchTerm?: string;
  sortType: string = '';
  sortReverse: boolean = false;

  countedRows?: number;
  isLoading: boolean = false;

  permissions: {
    viewArchive?: boolean;
    removeArchive?: boolean;
  } = { viewArchive: false, removeArchive: false };

  p: number = 1;
  user: any;

  storeDetails?: Shipping[] | null;
  assignedDriver?: Driver[] | null;
  assignedVehicle?: Vehicle[] | null;

  constructor(
    private destroyRef: DestroyRef,
    private shippingsService: ShippingsService,
    private bsModalService: BsModalService,
    public bsModalRef: BsModalRef,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService // used to get user information
  ) {
    this.loadPermissions();
  }

  private loadPermissions() {
    this.authCoreService.getPermissions().subscribe((permissions) => {
      const permissionsObject: Record<string, boolean> = {};
      permissions.forEach((permissions) => {
        const permissionName = permissions.permission_name;
        permissionsObject[permissionName] = true;
      });
      this.permissions = permissionsObject;
    });
  }

  ngOnInit(): void {
    this.fetchData();
    this.countRows();
  }

  private fetchData() {
    this.isLoading = true;
    setTimeout(() => {
      this.shippingsService.getShippingsArchive().then((shippingsRespone) => {
        this.shippings = shippingsRespone;
        this.isLoading = false;

        this.getStoreDetails();
        this.getAssignedDriverDetails();
        this.getAssignedVehicleDetails();
      });
    }, 2000);
  }

  private countRows() {
    this.shippingsService.getShippingsArchive().then((shippingsRespone) => {
      this.countedRows = shippingsRespone.length;
    });
  }

  reloadTable() {
    this.shippings = [];
    // this.countRows();
    this.fetchData();
  }

  sort(sortColumn: string) {
    if (this.sortType === sortColumn) {
      this.sortReverse = !this.sortReverse;
    } else {
      this.sortType = sortColumn;
      this.sortReverse = false;
    }

    this.shippings.sort((a: any, b: any) => {
      if (a[this.sortType] < b[this.sortType]) {
        return this.sortReverse ? 1 : -1;
      }
      if (a[this.sortType] > b[this.sortType]) {
        return this.sortReverse ? -1 : 1;
      }
      return 0;
    });
  }

  private getStoreDetails() {
    // here we are creating an array of requests to get client data for each active shipping
    const clientRequests = this.shippings.map((shipping) => this.shippingsService.getClientsById(shipping.store_id));

    // here we are using forkJoin to get all client data at once
    forkJoin(clientRequests)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (clientsData) => {
          this.shippings.forEach((shipping, index) => {
            if (clientsData[index]?.data?.length > 0) {
              shipping.storeDetails = clientsData[index].data[0];
            }
          });

          // At this point, each shipping object in this.activeShippings has an assignedClient property containing the client data
          // You can now use this data to render in your table
        },
      });
  }

  private getAssignedDriverDetails() {
    const driverRequests = this.shippings.map((shipping) => this.shippingsService.getDriversById(shipping.driver_id));
    forkJoin(driverRequests)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (driversData) => {
          this.shippings.forEach((shipping, index) => {
            if (driversData[index]?.data?.length > 0) {
              shipping.assignedDriver = driversData[index].data[0];
            }
          });
        },
      });
  }

  private getAssignedVehicleDetails() {
    const vehicleRequests = this.shippings.map((shipping) =>
      this.shippingsService.getVehiclesById(shipping.vehicle_id)
    );
    forkJoin(vehicleRequests)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (vehiclesData) => {
          this.shippings.forEach((shipping, index) => {
            if (vehiclesData[index]?.data?.length > 0) {
              shipping.assignedVehicle = vehiclesData[index].data[0];
            }
            console.log(shipping.assignedVehicle);
          });
        },
      });
  }

  openShippingDetails(selectedShipping: any) {
    this.bsModalRef = this.bsModalService.show(AddEditShippingModalComponent, {
      class: 'modal-xl',
      initialState: { viewDetails: true, shipping: selectedShipping },
    });
  }

  restoreShipping(shipping: Shipping) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: { action: 'Restore' },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.authCoreService
        .getUserInfo()
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (user: any) => {
            shipping.restored_by_id = user.id;
            shipping.restored_by_user = user.name;
            shipping.restored_at = new Date().toISOString();
            this.shippingsService.newshipping(shipping).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Driver not restored');
              } else {
                this.shippingsService.removeShippingFromArchive(shipping.id).then((res) => {
                  if (res.error) {
                    this.toastr.error(
                      `${res.error?.details}`,
                      'Driver not was restored but not deleted from the archive'
                    );
                    this.toastr.info('Please contact the System Administrator', 'Info!');
                  } else {
                    this.toastr.success('Driver restored successfully', 'Success!');
                    this.reloadTable();
                    this.bsModalRef.hide();
                  }

                  this.reloadTable();
                  this.bsModalRef.hide();
                });
                this.toastr.success('Driver restored successfully', 'Success!');
                this.reloadTable();
                this.bsModalRef.hide();
              }
            });
          },
        });
    });
    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }

  deleteShipping(shipping: Shipping) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: {
        action: 'Delete',
        message: '!!! ATENTION this action is NOT REVERSIBLE. ALL THE DATA WILL BE LOST FOREVER!',
      },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.shippingsService.removeShippingFromArchive(shipping.id).then((res) => {
        if (res.error) {
          this.toastr.error(`${res.error?.details}`, 'Shipping not deleted');
        } else {
          this.toastr.success('Shipping deleted', 'Success!');
          this.reloadTable();
          this.bsModalRef.hide();
        }
      });
    });

    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }
}

import { NgClass, NgIf, NgTemplateOutlet } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IconButtonComponent } from '@jurca-raul/components/icon-button';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'jr-modal',
  templateUrl: 'modal.component.html',
  styleUrls: ['modal.component.scss'],
  standalone: true,
  imports: [NgIf, NgTemplateOutlet, NgClass, IconButtonComponent],
})
export class ModalComponent {
  @Input()
  displayTopRightCloseButton: boolean = true;
  @Input() displayHeader: boolean = true;
  @Input() noPadding: boolean = false;

  @Output()
  closeModal: EventEmitter<void> = new EventEmitter<void>();
}

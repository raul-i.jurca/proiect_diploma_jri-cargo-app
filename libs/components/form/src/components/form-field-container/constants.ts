export const enum PositionEnum {
  RIGHT = 'RIGHT',
  LEFT = 'LEFT',
}

export const FormFieldLayout = {
  VERTICAL: 'vertical',
  HORIZONTAL: 'horizontal',
} as const;

export type FormFieldLayoutType = (typeof FormFieldLayout)[keyof typeof FormFieldLayout];

export const FormFieldSize = {
  NORMAL: 'normal',
} as const;

export type FormFieldSizeType = (typeof FormFieldSize)[keyof typeof FormFieldSize];

export enum ArchiveTabEnum {
  VEHICLES = 'Vehicles',
  DRIVERS = 'Drivers',
  DELIVERIES = 'Deliveries',
  STORES = 'Stores',
}

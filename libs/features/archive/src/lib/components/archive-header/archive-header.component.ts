import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { TabNavComponent } from '@jurca-raul/components/tab-nav';
import { RouterLinkService } from '@jurca-raul/services/router';
import { TabNavElement } from 'libs/components/tab-nav/src/lib/domain';
import { ArchiveTabEnum } from '../../domain';

@Component({
  selector: 'jr-archive-header',
  templateUrl: './archive-header.component.html',
  styleUrls: ['./archive-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [TabNavComponent],
})
export class ArchiveHeaderComponent implements OnInit {
  @Input({ required: true }) activeTab?: string;
  @Input({ required: true }) badge: string = '';

  tabs: TabNavElement[] = [];

  constructor(private routerLinkService: RouterLinkService) {}

  ngOnInit(): void {
    this.defineTabs();
  }
  private defineTabs(): void {
    this.tabs = [
      {
        label: 'Vehicles',
        active: this.activeTab === ArchiveTabEnum.VEHICLES,
        link: this.routerLinkService.routerLink.vehiclesArchive,
        tooltip: 'Archived vehicles',
      },
      {
        label: 'Drivers',
        active: this.activeTab === ArchiveTabEnum.DRIVERS,
        link: this.routerLinkService.routerLink.driversArchive,
        tooltip: 'Archived drivers',
      },
      {
        label: 'Deliveries',
        active: this.activeTab === ArchiveTabEnum.DELIVERIES,
        link: this.routerLinkService.routerLink.deliveriesArchive,
        tooltip: 'Archived deliveries',
      },
      {
        label: 'Stores',
        active: this.activeTab === ArchiveTabEnum.STORES,
        link: this.routerLinkService.routerLink.storesArchive,
        tooltip: 'Archived stores',
      },
    ];
  }
}

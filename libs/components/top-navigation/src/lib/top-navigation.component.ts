import { Component, Input, Inject, OnInit } from '@angular/core';
import { CommonModule, DOCUMENT } from '@angular/common';
import { AuthService } from '@auth0/auth0-angular';
import { faUser, faPowerOff } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbCollapseModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ButtonComponent } from '@jurca-raul/components/button';
import { HttpClient } from '@angular/common/http';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'jurca-raul-top-nav',
  templateUrl: './top-navigation.component.html',
  styleUrls: ['./top-navigation.component.scss'],
  standalone: true,
  imports: [CommonModule, FontAwesomeModule, NgbCollapseModule, NgbModule, ButtonComponent],
})
export class TopNavigationComponent implements OnInit {
  @Input() logo = 'assets/images/jri-market-logistic-high-resolution-logo-transparent.svg';
  isCollapsed = true;

  faUser = faUser;
  faPowerOff = faPowerOff;

  constructor(
    public auth: AuthService,
    public http: HttpClient,
    @Inject(DOCUMENT) private doc: Document
  ) {}
  token: any;

  ngOnInit() {
    this.auth.idTokenClaims$.subscribe((data) => {
      this.token = data;
      console.log(this.token);
    });
  }

  loginWithRedirect() {
    this.auth.loginWithRedirect();
  }
  logout() {
    this.auth.logout({ logoutParams: { returnTo: this.doc.location.origin } });
  }
}

import { createClient } from '@supabase/supabase-js';
import config from '../../auth_config.json';

const {
  domain,
  clientId,
  authorizationParams: { audience },
  apiUri,
  errorPath,
} = config as {
  domain: string;
  clientId: string;
  authorizationParams: {
    audience?: string;
  };
  apiUri: string;
  errorPath: string;
};

export const environment = {
  production: false,
  auth: {
    domain,
    clientId,
    authorizationParams: {
      ...(audience && audience !== 'https://rauljurca.eu.auth0.com/api/cargo-app' ? { audience } : null),
      redirect_uri: window.location.origin,
    },
    errorPath,
  },
  httpInterceptor: {
    allowedList: [`${apiUri}/*`],
  },
  supaBase: {
    url: 'https://sebjnvxcsucwkngrclut.supabase.co',
    key: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InNlYmpudnhjc3Vjd2tuZ3JjbHV0Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODEzMTE1MzIsImV4cCI6MTk5Njg4NzUzMn0.inZsntZJaiIZpyXJHlTEBlxQSzPc1qTtqEbKyuA7tjM',
  },
};

export const supabaseDb = createClient(environment.supaBase.url, environment.supaBase.key);

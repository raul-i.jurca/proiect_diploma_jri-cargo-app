import { Routes } from '@angular/router';
import { ActiveShippingsComponent } from './components/active-shippings/active-shippings.component';
import { CompletedShippingsComponent } from './components/completed-shippings/completed-shippings.component';

export const routes: Routes = [
  { path: 'active', component: ActiveShippingsComponent, data: { title: 'Active Shippings' } },
  { path: 'completed', component: CompletedShippingsComponent, data: { title: 'Completed Shippings' } },
  { path: '', redirectTo: 'active', pathMatch: 'full' },
];
export default routes;

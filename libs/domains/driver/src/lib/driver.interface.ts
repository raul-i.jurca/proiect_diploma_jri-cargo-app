export interface Driver {
  id: string;
  s_name: string;
  f_name: string;
  date_of_birth: string;

  phone: number;
  email: string;

  city: string;
  county: string;
  street: string;
  street_number: number;

  id_document_type: string;
  id_series: string;
  national_id_number: string;
  national_id_issued_by: string;

  national_id_issued_at: string;
  national_id_expires_at: string;
  national_identifier: number;

  emergency_contact_name: string;
  emergency_contact_phone: number;

  comments: string;

  created_at: string;
  created_by_id?: string;
  created_by_name?: string;

  edited_at?: string;
  edited_by_id?: string;
  edited_by_name?: string;

  deleted_at?: string;
  deleted_by_id?: string;
  deleted_by_name?: string;

  restored_at?: string;
  restored_by_id?: string;
  restored_by_name?: string;
}

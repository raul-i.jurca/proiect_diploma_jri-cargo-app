import { Injectable } from '@angular/core';
import { supabaseDb } from '@jurca-raul/environment';

@Injectable({
  providedIn: 'root',
})
export class VehiclesService {
  getVehicles() {
    return supabaseDb
      .from('vehicles')
      .select('*')
      .then((res: any) => res.data);
  }

  getvehiclesArchive() {
    return supabaseDb
      .from('deleted_vehicles')
      .select('*')
      .then((res: any) => res.data);
  }

  // methods for drivers table
  newVehicle(vehicle: any) {
    return supabaseDb.from('vehicles').insert([vehicle]).select();
  }

  updateVehicle(vehicles: any, id: string) {
    return supabaseDb.from('vehicles').update(vehicles).eq('id', id).select();
  }

  deleteVehicle(id: string) {
    return supabaseDb.from('vehicles').delete().eq('id', id).select();
  }

  moveVehicleToArchive(vehicle: any) {
    return supabaseDb.from('deleted_vehicles').insert([vehicle]).select();
  }

  removeVehicleFromArchive(id: string) {
    return supabaseDb.from('deleted_vehicles').delete().eq('id', id).select();
  }
}

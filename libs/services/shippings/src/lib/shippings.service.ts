import { Injectable } from '@angular/core';
import { supabaseDb } from '@jurca-raul/environment';

@Injectable({
  providedIn: 'root',
})
export class ShippingsService {
  getData() {
    return supabaseDb
      .from('shippings')
      .select('*')
      .then((res: any) => res.data);
  }

  getShippingsArchive() {
    return supabaseDb
      .from('deleted_shippings')
      .select('*')
      .then((res: any) => res.data);
  }

  getActiveShippings() {
    return supabaseDb.from('shippings').select('*').eq('is_completed', false);
  }

  getCompletedShippings() {
    return supabaseDb.from('shippings').select('*').eq('is_completed', true);
  }

  getClientsById(id: string) {
    return supabaseDb.from('stores').select('*').eq('id', id);
  }

  getDriversById(id: string) {
    return supabaseDb.from('drivers').select('*').eq('id', id);
  }

  getVehiclesById(id: string) {
    return supabaseDb.from('vehicles').select('*').eq('id', id);
  }

  newshipping(shipping: any) {
    return supabaseDb.from('shippings').insert([shipping]).select();
  }

  markVehiclesOnNewShipping(id: string, isAvailable: boolean) {
    return supabaseDb.from('vehicles').update({ is_available: isAvailable }).eq('id', id).select();
  }

  markStoreOnNewShipping(id: string, isAvailable: boolean) {
    return supabaseDb.from('stores').update({ is_available: isAvailable }).eq('id', id).select();
  }

  markDriverOnNewShipping(id: string, isAvailable: boolean) {
    return supabaseDb.from('drivers').update({ is_available: isAvailable }).eq('id', id).select();
  }

  updateshippings(shipping: any, id: string) {
    return supabaseDb.from('shippings').update(shipping).eq('id', id).select();
  }

  deleteShipping(id: string) {
    return supabaseDb.from('shippings').delete().eq('id', id).select();
  }

  moveshippingToArchive(shipping: any) {
    return supabaseDb.from('deleted_shippings').insert([shipping]).select();
  }
  removeShippingFromArchive(id: string) {
    return supabaseDb.from('deleted_shippings').delete().eq('id', id).select();
  }
}

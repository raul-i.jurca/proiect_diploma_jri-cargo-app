export type IconSize = 'SMALL' | 'MEDIUM' | 'LARGE' | 'X_LARGE';

export const IconSizes: Record<IconSize, string> = {
  SMALL: 'fa-xs',
  MEDIUM: 'fa-1x',
  LARGE: 'fa-xl',
  X_LARGE: 'fa-2x',
} as const;

export type IconStyle = 'ICON';

export type IconAnimation = 'BEAT' | 'FADE' | 'BEAT_FADE' | 'BOUNCE' | 'FLIP' | 'SHAKE' | 'SPIN' | 'UNSET';

export const IconAnimations: Record<IconAnimation, string> = {
  BEAT: 'fa-beat',
  FADE: 'fa-fade',
  BEAT_FADE: 'fa-beat-fade',
  BOUNCE: 'fa-bounce',
  FLIP: 'fa-flip',
  SHAKE: 'fa-shake',
  SPIN: 'fa-spin',
  UNSET: '',
} as const;

export enum IconSetEnum {
  //new
  UNSET = '',

  USER_PLUS = 'fa-solid fa-user-plus',
  SETINGS = 'fa-solid fa-gear',
  PLUS = 'fa-solid fa-plus',
  CLOSE = 'fa-solid fa-xmark',
  TRASH = 'fa-solid fa-trash',
  PEN = 'fa-solid fa-pen',
  SAVE = 'fa-solid fa-floppy-disk',
  LOG_OUT = 'fa-solid fa-sign-out',
  CHEVRON_DOWN = 'fa-solid fa-chevron-down',
  CHEVRON_RIGHT = 'fa-solid fa-chevron-right',
  SEARCH = 'fa-solid fa-magnifying-glass',
  EYE = 'fa-solid fa-eye',
  RESTORE = 'fa-solid fa-rotate-left',
  CHECK = 'fa-solid fa-check',
  SPINNER = 'fa-solid fa-spinner',
  PDF = 'fa-solid fa-file-pdf',

  UP_ZA = 'fa-solid fa-arrow-up-z-a',
  DOWN_AZ = 'fa-solid fa-arrow-down-a-z',

  UP_91 = 'fa-solid fa-arrow-up-9-1',
  DOWN_19 = 'fa-solid fa-arrow-down-1-9',

  XCIRCLE = 'fa-solid fa-circle-xmark',
  CHECK_CIRCLE = 'fa-solid fa-circle-check',

  SORT_UP = 'fa-solid fa-arrow-up-wide-short',
  SORT_DOWN = 'fa-solid fa-arrow-down-wide-short',

  // SOCIAL
  TWITTER = 'fa-brands fa-x-twitter',
  LINKEDIN = 'fa-brands fa-linkedin',
  GITHUB = 'fa-brands fa-github',
  FACEBOOK = 'fa-brands fa-facebook',
}

export type Icon = keyof typeof IconSetEnum;

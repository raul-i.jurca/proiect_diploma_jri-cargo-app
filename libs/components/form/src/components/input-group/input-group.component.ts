import { AsyncPipe, NgFor, NgIf } from '@angular/common';
import {
  Component,
  ContentChild,
  ContentChildren,
  EventEmitter,
  Input,
  Output,
  QueryList,
} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormFieldContainerComponent } from '../form-field-container';
import {
  FormFieldLayout,
  FormFieldLayoutType,
  FormFieldSize,
  FormFieldSizeType,
} from '../form-field-container/constants';
import {
  InputDirective,
  PrefixDirective,
  SuffixDirective,
} from '../directives';
import { FormLabelComponent } from '../label';
import { InputTypes } from './input.models';

@Component({
  selector: 'jr-input-group',
  templateUrl: './input-group.component.html',
  styleUrls: ['./input-group.component.scss'],
  standalone: true,
  imports: [
    NgIf,
    NgFor,
    AsyncPipe,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule,
    FormFieldContainerComponent,
    FormLabelComponent,
    SuffixDirective,
    PrefixDirective,
    InputDirective,
  ],
})
export class InputGroupComponent {
  @Input() layout: FormFieldLayoutType = FormFieldLayout.VERTICAL;
  @Input() size: FormFieldSizeType = FormFieldSize.NORMAL;
  @Input() name: string = '';
  @Input() label: string = '';
  @Input() clearable = false;
  @Input() showRequiredMark = false;
  @Input() allowPasswordVisibilityToggle = false;

  @Input() errors: string[] = [];
  @Input() helpers: string[] = [];
  @Input() warnings: string[] = [];

  @Input() labelContainerClass: string = '';
  @Input() inputContainerClass: string = '';

  @Output() inputCleared: EventEmitter<void> = new EventEmitter();

  @ContentChildren(PrefixDirective, { descendants: true })
  prefixChildren!: QueryList<PrefixDirective>;
  @ContentChildren(SuffixDirective, { descendants: true })
  suffixChildren!: QueryList<SuffixDirective>;
  @ContentChild(InputDirective) inputControl: InputDirective | undefined;

  get hasPrefix(): boolean {
    return this.prefixChildren?.length > 0 || this.isInputTypeSearch();
  }

  get hasSuffix(): boolean {
    return this.suffixChildren?.length > 0 || this.isInputTypeSearch();
  }

  get inputFocused(): boolean {
    if (this.inputControl) {
      return this.inputControl.isFocused;
    }

    return false;
  }

  get controlValue(): string {
    return this.inputControl?.ngControl?.value;
  }

  isSearchIconVisible() {
    return this.isInputTypeSearch() && this.prefixChildren.length === 0;
  }

  isInputClearable() {
    return this.isInputTypeSearch() || this.clearable;
  }

  isInputTypeEmail(): boolean {
    return this.inputControl?.inputType === InputTypes.Email;
  }

  isInputTypeSearch(): boolean {
    return this.inputControl?.inputType === InputTypes.Search;
  }

  clearInput(): void {
    this.inputControl?.clearInput();
    this.inputCleared.emit();
  }

  togglePasswordVisibility(): void {
    this.inputControl?.togglePasswordVisibility();
  }
}

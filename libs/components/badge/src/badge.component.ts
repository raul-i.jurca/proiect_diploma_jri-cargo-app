import { Component, Input } from '@angular/core';

@Component({
  selector: 'jr-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss'],
  standalone: true,
  imports: [],
})
export class BadgeComponent {
  @Input({ required: true }) badgeText: string = 'Badge';

  constructor() {}
}

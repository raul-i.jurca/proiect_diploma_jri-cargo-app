import { Component, DestroyRef, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { SupabaseService } from '@jurca-raul/services/supabase';
import { DatePipe, NgFor, NgIf } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { ToastrService } from 'ngx-toastr';
import { ModalComponent } from '@jurca-raul/modals/modal';
import { ButtonComponent } from '@jurca-raul/components/button';
import { InputDirective, InputGroupComponent } from '@jurca-raul/components/form';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Driver } from '@jurca-raul/domains/driver';
import { DropdownComponent, DropdownOption } from '@jurca-raul/components/dropdown';
import { Vehicle } from '@jurca-raul/domains/vehicle';
import { NgPipesModule } from 'ngx-pipes';
import { ShippingsService } from '@jurca-raul/services/shippings';
import { Shipping } from '@jurca-raul/domains/shipping';
import { FormLabelComponent } from 'libs/components/form/src/components/label';
import { Stores } from '@jurca-raul/domains/stores';

@Component({
  templateUrl: './add-edit-shipping-modal.component.html',
  styleUrls: ['./add-edit-shipping-modal.component.scss'],
  standalone: true,
  providers: [DatePipe],
  imports: [
    NgIf,
    NgFor,
    ReactiveFormsModule,
    FormsModule,
    ModalComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    BsDatepickerModule,
    DropdownComponent,
    NgPipesModule,
    FormLabelComponent,
  ],
})
export class AddEditShippingModalComponent {
  @Output() tableRefresh: EventEmitter<any> = new EventEmitter();

  viewDetails: boolean = false;

  newDelivery: FormGroup = new FormGroup({});

  storeLabel: string = 'Select a store';
  driverLabel: string = 'Select a driver';
  vehicleLabel: string = 'Select a vehicle';

  deliveries: any[] = [];
  shipping?: Shipping;

  stores: DropdownOption[] = [];
  availableDrivers: DropdownOption[] = [];
  availableVehicles: DropdownOption[] = [];

  user: any;

  modalRef?: BsModalRef;
  message?: string;
  hasChanges: boolean = false;

  constructor(
    private destroyRef: DestroyRef,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    public bsModalRef: BsModalRef,
    private shippingsService: ShippingsService,
    private supabaseService: SupabaseService,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService // private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.buildForm();
    this.populateDropdowns();
  }

  buildForm() {
    this.newDelivery = this.formBuilder.group({
      driver_id: new FormControl(
        { value: this.shipping ? this.shipping.assignedDriver.id : null, disabled: false },
        Validators.required
      ),
      store_id: new FormControl(
        { value: this.shipping ? this.shipping.storeDetails.id : null, disabled: false },
        Validators.required
      ),
      vehicle_id: new FormControl(
        { value: this.shipping ? this.shipping.assignedVehicle.id : null, disabled: false },
        Validators.required
      ),
    });
  }

  onDriverSelect(value: DropdownOption): void {
    this.newDelivery.patchValue({ driver_id: value.optionValue });
    this.driverLabel = value.displayName;
  }
  onClientSelect(value: DropdownOption): void {
    this.newDelivery.patchValue({ store_id: value.optionValue });
    this.storeLabel = value.displayName;
  }

  onVehicleSelect(value: DropdownOption): void {
    this.newDelivery.patchValue({ vehicle_id: value.optionValue });
    this.vehicleLabel = value.displayName;
  }

  private populateDropdowns() {
    this.supabaseService.getAvailableData('drivers').then((driversResponse) => {
      this.availableDrivers = driversResponse.map((driver: Driver) => ({
        displayName: driver.s_name + ' ' + driver.f_name,
        optionValue: driver.id,
      }));
    });

    this.supabaseService.getAvailableData('stores').then((storesResponse) => {
      this.stores = storesResponse.map((store: Stores) => ({
        displayName: store.store_name,
        optionValue: store.id,
      }));
    });

    this.supabaseService.getAvailableData('vehicles').then((vehiclesResponse) => {
      this.availableVehicles = vehiclesResponse.map((vehicle: Vehicle) => ({
        displayName: vehicle.registration_number,
        optionValue: vehicle.id,
      }));
    });
  }

  onSave(newDelivery: FormGroup): void {
    console.log(newDelivery);
    if (!this.newDelivery.valid) {
      this.toastr.error('Please fill all the fields', 'Error!');
      return;
    }
    this.modalRef = this.modalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
    });
    this.modalRef.content.Confirm.subscribe(() => {
      this.saveNewShipping(newDelivery);
      this.modalRef?.hide();
    });
    this.modalRef.content.Decline.subscribe(() => {
      this.modalRef?.hide();
    });
    console.log(newDelivery.value);
  }

  saveNewShipping(newDelivery: FormGroup) {
    this.authCoreService
      .getUserInfo()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (user: any) => {
          if (!this.shipping) {
            if (newDelivery.valid) {
              const shipping = {
                created_at: new Date().toISOString(),
                created_by_id: user.id,
                created_by_user: user.name,

                driver_id: newDelivery.value.driver_id,
                store_id: newDelivery.value.store_id,
                vehicle_id: newDelivery.value.vehicle_id,
              };

              Promise.all([
                this.supabaseService.markDriverUnavailableOnNewShipping(newDelivery.value.driver_id, false),
                this.supabaseService.makeStoreUnavailableOnNewShipping(newDelivery.value.store_id, false),
                this.supabaseService.makeVehiclesUnavailableOnNewShipping(newDelivery.value.vehicle_id, false),
              ]).then((responses) => {
                const errorResponse = responses.find((res) => res.error);

                if (errorResponse) {
                  this.toastr.error(`${errorResponse.error?.details}`, 'Shipping not created');
                } else {
                  this.shippingsService.newshipping(shipping).then((res) => {
                    if (res.error) {
                      this.toastr.error(`${res.error?.details}`, 'Shipping not updated');
                    } else {
                      this.tableRefresh.emit();
                      this.toastr.success('Shipping added successfully', 'Success!');
                    }
                  });
                }
              });

              this.bsModalRef.hide();
            } else {
              this.toastr.error('Please fill all the fields', 'Error!');
            }
          } else {
            const shipping = {
              updated_at: new Date().toISOString(),
              updated_by_id: user.id,
              updated_by_user: user.name,

              driver_id: newDelivery.value.driver_id,
              store_id: newDelivery.value.store_id,
              vehicle_id: newDelivery.value.vehicle_id,
            };
            this.shippingsService.updateshippings(shipping, this.shipping.id).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Shipping not updated');
              } else {
                this.toastr.success('Shipping updated successfully', 'Success!');
                this.tableRefresh.emit();
                this.bsModalRef.hide();
              }
            });
          }
        },
      });
  }
}

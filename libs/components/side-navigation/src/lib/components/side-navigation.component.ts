import { Component } from '@angular/core';
import { NavigationItem, SIDE_NAVIGATION_ITEMS } from '../domain';
import { RouterLinkService } from '@jurca-raul/services/router';
import { NgClass, NgFor } from '@angular/common';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { IconButtonComponent } from '@jurca-raul/components/icon-button';

enum SocialLinksEnum {
  LINKEDIN = 'https://www.linkedin.com/in/raul-jurca-0801211b5/',
  TWITTER = 'https://twitter.com/JurcaRaulIonut',
  GITHUB = 'https://github.com/jurcaraul',
}

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'jurca-raul-side-nav',
  templateUrl: './side-navigation.component.html',
  styleUrls: ['./side-navigation.component.scss'],
  standalone: true,
  imports: [NgFor, NgClass, RouterLink, RouterLinkActive, IconButtonComponent],
})
export class SideNavigationComponent {
  navigationItems: NavigationItem[];

  socialLinksEnum = SocialLinksEnum;

  constructor(public routerLinkService: RouterLinkService) {
    this.navigationItems = SIDE_NAVIGATION_ITEMS(this.routerLinkService);
  }

  openProfile(socialLink: SocialLinksEnum): void {
    window.open(socialLink, '_blank');
    console.log(socialLink);
  }
}

import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterOutlet } from '@angular/router';
import { TopNavigationComponent } from '@jurca-raul/components/top-navigation';
import { SideNavigationComponent } from '@jurca-raul/components/side-navigation';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { User } from '@auth0/auth0-angular';
import { BsModalService } from 'ngx-bootstrap/modal';
import { EmailConfirmationModalComponent } from '../email-confirmation-modal';
import { filter } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'jurca-raul-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  standalone: true,
  imports: [RouterOutlet, TopNavigationComponent, SideNavigationComponent],
})
export class MainComponent {
  user: User;

  constructor(
    private authCoreService: AuthCoreService,
    private bsModalService: BsModalService,
    private titleService: Title,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe(() => {
      this.updateTitle();
    });
  }

  ngOnInit(): void {
    this.authCoreService.getUserInfo().subscribe((data) => {
      this.user = data;
      if (this.user.email_verified) {
        return;
      }
      this.bsModalService.show(EmailConfirmationModalComponent);
    });
  }

  private updateTitle() {
    const pageTitle = this.getPageTitle(this.activatedRoute);
    this.titleService.setTitle(pageTitle);
  }

  private getPageTitle(route: ActivatedRoute): string {
    let title = '';
    while (route.firstChild) {
      route = route.firstChild;
      if (route.snapshot.data && route.snapshot.data.title) {
        title = 'JRI Market | Logistics' + ' -> ' + route.snapshot.data.title;
      }
    }
    return title;
  }
}

import { Routes } from '@angular/router';
import { VehiclesComponent } from './components/vehicles.component';

export const routes: Routes = [{ path: '', component: VehiclesComponent, data: { title: 'Vehicles' } }];
export default routes;

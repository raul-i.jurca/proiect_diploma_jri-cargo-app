import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[JrClickStopPropagation]',
  standalone: true,
})
export class ClickStopPropagationDirective {
  @HostListener('click', ['$event'])
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public onClick(event: any): void {
    event.stopPropagation();
    event.preventDefault();
  }
}

import { Routes } from '@angular/router';
import { AdministrationComponent } from './components';

export const routes: Routes = [{ path: '', component: AdministrationComponent }];
export default routes;

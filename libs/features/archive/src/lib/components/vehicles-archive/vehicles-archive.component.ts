import { Component, DestroyRef } from '@angular/core';
import { ArchiveTabEnum } from '../../domain';
import { NgIf, NgFor } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ButtonComponent } from '@jurca-raul/components/button';
import { InputGroupComponent, InputDirective, SuffixDirective } from '@jurca-raul/components/form';
import { IconComponent } from '@jurca-raul/components/icon';
import { IconButtonComponent } from '@jurca-raul/components/icon-button';
import { TablePreloaderComponent } from '@jurca-raul/components/table-preloader';
import { AuthCoreService } from '@jurca-raul/core-auth';
import { Vehicle } from '@jurca-raul/domains/vehicle';
import { AddEditVehicleModalComponent } from '@jurca-raul/modals/add-edit-view-vehicle';
import { ConfirmModalComponent } from '@jurca-raul/modals/confirm';
import { JrDatePipe } from '@jurca-raul/pipes/date';
import { VehiclesService } from '@jurca-raul/services/vehicle';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgArrayPipesModule } from 'ngx-pipes';
import { ToastrService } from 'ngx-toastr';
import { ArchiveHeaderComponent } from '../archive-header';

@Component({
  selector: 'jr-vehicles-archive',
  templateUrl: './vehicles-archive.component.html',
  standalone: true,
  imports: [
    ArchiveHeaderComponent,
    NgIf,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    NgFor,
    JrDatePipe,
    IconComponent,
    TooltipModule,
    IconButtonComponent,
    ButtonComponent,
    InputGroupComponent,
    InputDirective,
    SuffixDirective,
    IconComponent,
    NgArrayPipesModule,
    InputGroupComponent,
    TablePreloaderComponent,
  ],
})
export class VehiclesArchiveComponent {
  readonly noPermissonsMessage = 'You do not have permissions to perform this action';

  archiveTabEnum = ArchiveTabEnum;

  vehicles: Vehicle[] = [];

  searchTerm?: string;
  sortType: string = '';
  sortReverse: boolean = false;

  countedRows?: number;
  isLoading: boolean = false;

  permissions: {
    viewArchive?: boolean;
    removeArchive?: boolean;
  } = { viewArchive: false, removeArchive: false };

  p: number = 1;
  user: any;

  constructor(
    private destroyRef: DestroyRef,
    private vehiclesService: VehiclesService,
    private bsModalService: BsModalService,
    public bsModalRef: BsModalRef,
    private toastr: ToastrService,
    private authCoreService: AuthCoreService // used to get user information
  ) {
    this.loadPermissions();
  }

  ngOnInit(): void {
    this.countRows();
    this.fetchData();
  }

  private loadPermissions() {
    this.authCoreService.getPermissions().subscribe((permissions) => {
      const permissionsObject: Record<string, boolean> = {};
      permissions.forEach((permissions) => {
        const permissionName = permissions.permission_name;
        permissionsObject[permissionName] = true;
      });
      this.permissions = permissionsObject;
    });
  }

  private fetchData() {
    this.isLoading = true;
    setTimeout(() => {
      this.vehiclesService.getvehiclesArchive().then((vehiclesResponse) => {
        this.vehicles = vehiclesResponse;
        console.log(this.vehicles);
        this.isLoading = false;
      });
    }, 2000);
  }

  reloadTable() {
    this.vehicles = [];
    this.countRows();
    this.fetchData();
  }

  async countRows() {
    this.vehiclesService.getvehiclesArchive().then((driversDB) => {
      this.countedRows = driversDB.length;
    });
  }

  sort(sortColumn: string) {
    if (this.sortType === sortColumn) {
      this.sortReverse = !this.sortReverse;
    } else {
      this.sortType = sortColumn;
      this.sortReverse = false;
    }

    this.vehicles.sort((a: any, b: any) => {
      if (a[this.sortType] < b[this.sortType]) {
        return this.sortReverse ? 1 : -1;
      }
      if (a[this.sortType] > b[this.sortType]) {
        return this.sortReverse ? -1 : 1;
      }
      return 0;
    });
  }

  openVehicleProfile(vehicle: Vehicle) {
    this.bsModalRef = this.bsModalService.show(AddEditVehicleModalComponent, {
      class: 'modal-lg',
      initialState: { vehicle: vehicle, viewDetails: true },
    });
  }

  restoreVehicle(vehicle: Vehicle) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: { action: 'Restore', param1: vehicle.registration_number },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.authCoreService
        .getUserInfo()
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (user: any) => {
            vehicle.restored_by_id = user.id;
            vehicle.restored_by_user = user.name;
            vehicle.restored_at = new Date().toISOString(); // this can be also handled by supabase
            this.vehiclesService.newVehicle(vehicle).then((res) => {
              if (res.error) {
                this.toastr.error(`${res.error?.details}`, 'Vehicle not restored');
              } else {
                this.vehiclesService.removeVehicleFromArchive(vehicle.id).then((res) => {
                  if (res.error) {
                    this.toastr.error(`${res.error?.details}`, 'Vehicle was restored but not deleted from the archive');
                    this.toastr.info('Please contact the System Administrator', 'Info!');
                  } else {
                    this.toastr.success('Vehicle restored successfully', 'Success!');
                    this.reloadTable();
                    this.bsModalRef.hide();
                  }
                });
                this.toastr.success('Vehicle restored successfully', 'Success!');
                this.reloadTable();
                this.bsModalRef.hide();
              }
            });
          },
        });
    });
    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }

  deleteVehicle(vehicle: Vehicle) {
    this.bsModalRef = this.bsModalService.show(ConfirmModalComponent, {
      class: 'modal-sm',
      animated: true,
      initialState: {
        action: 'Delete',
        message: '!!! ATENTION this action is NOT REVERSIBLE. ALL THE DATA WILL BE LOST FOREVER!',
      },
    });

    this.bsModalRef.content.Confirm.subscribe(() => {
      this.vehiclesService.removeVehicleFromArchive(vehicle.id).then((res) => {
        if (res.error) {
          this.toastr.error(`${res.error?.details}`, 'Vehicle not deleted');
        } else {
          this.toastr.success('Vehicle deleted', 'Success!');
          this.reloadTable();
          this.bsModalRef.hide();
        }
      });
    });

    this.bsModalRef.content.Decline.subscribe(() => {
      this.bsModalRef.hide();
      this.toastr.info('Action canceled', 'Info!');
    });
  }
}

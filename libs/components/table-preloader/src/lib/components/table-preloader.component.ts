import { NgFor } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SkeletonModule } from 'primeng/skeleton';

@Component({
  selector: 'jr-table-preloader',
  templateUrl: './table-preloader.component.html',
  styleUrls: ['./table-preloader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgFor, SkeletonModule],
})
export class TablePreloaderComponent {
  @Input() rows: number = 10;
  // @Input() cols: number = 7;

  get rowsArray(): number[] {
    return new Array(this.rows);
  }
  // get colsArray(): number[] {
  //   return new Array(this.cols);
  // }
}
